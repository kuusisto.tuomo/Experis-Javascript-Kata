export function anagrams(word, wordArray){
    if(typeof(word) != 'string' || word.length < 1 || !Array.isArray(wordArray) || wordArray.length < 1) throw Error("Input is not valid.");
    const sortedWord = JSON.stringify(Array.from(word.toLowerCase()).sort());
    return wordArray.filter(
        arrayWord => (sortedWord === JSON.stringify(arrayWord.toLowerCase().split("").sort()))
    )
}