import {anagrams} from ".";

test(`('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa']`, () => {
    expect(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada'])).toStrictEqual(['aabb', 'bbaa']);
})

test(`('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer']`, () => {
    expect(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer'])).toStrictEqual(['carer', 'racer']);
})

test(`('laser', ['lazing', 'lazy', 'lacer']) => []`, () => {
    expect(anagrams('laser', ['lazing', 'lazy', 'lacer'])).toStrictEqual([]);
})

test(`('123', ['321', '213', '1234', '12']) => ['321', '213']`, () => {
    expect(anagrams('123', ['321', '213', '1234', '12'])).toStrictEqual(['321', '213']);
})

test(`('a', ['a','b','c','d']) => ['a']`, () => {
    expect(anagrams('a', ['a','b','c','d'])).toStrictEqual(['a']);
})

test(`('hello world', ['doll howler', 'droll whole', 'Howell lord', 'holler wold']) => ['doll howler', 'droll whole', 'Howell lord', 'holler wold']`, () => {
    expect(anagrams('hello world', ['doll howler', 'droll whole', 'Howell lord', 'holler wold'])).toStrictEqual(['doll howler', 'droll whole', 'Howell lord', 'holler wold']);
})

test(`['hello', 'world'], ['hello'] => Error`, () => {
    expect(() => anagrams(['hello', 'world'], ['hello'])).toThrowError();
})

test(`hello, hello => Error`, () => {
    expect(() => anagrams('hello', 'hello')).toThrowError();
})

test(`'', ['hello'] => Error`, () => {
    expect(() => anagrams('', ['hello'])).toThrowError();
})

test(`'hello', [] => Error`, () => {
    expect(() => anagrams('hello', [])).toThrowError();
})