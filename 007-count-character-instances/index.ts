export function charCount(char, sentence){
    if(char.length != 1) throw Error("First argument should be a single char")
    let count = 0
    for(let i = 0; i < sentence.length; ++i)
    {
        sentence.charAt(i) === char && ++count
    }

    return count
}