// charCount("a", "edabit") ➞ 1
// charCount("c", "Chamber of secrets") ➞ 1
// charCount("B", "boxes are fun") ➞ 0
// charCount("b", "big fat bubble") ➞ 4
// charCount("e", "javascript is good") ➞ 0
// charCount("!", "!easy!") ➞ 2
// charCount("wow", "the universe is wow") ➞ error

const string1 = `"a", "edabit"`
const string2 = `"c", "Chamber of secrets"`
const string3 = `"B", "boxes are fun"`
const string4 = `"b", "big fat bubble""`
const string5 = `"e", "javascript is good"`
const string6 = `"!", "!easy!"`
const string7 = `"wow", "the universe is wow"`


import {charCount} from "."

test(`"a", "edabit" ➞ 1`, () => {
    expect(charCount("a", "edabit")).toBe(1)
})

test(`"c", "Chamber of secrets" ➞ 1`, () => {
    expect(charCount("c", "Chamber of secrets")).toBe(1)
})

test(`"B", "boxes are fun" ➞ 0`, () => {
    expect(charCount("B", "boxes are fun")).toBe(0)
})

test(`"b", "big fat bubble" ➞ 4`, () => {
    expect(charCount("b", "big fat bubble")).toBe(4)
})

test(`"e", "javascript is good" ➞ 0`, () => {
    expect(charCount("e", "javascript is good")).toBe(0)
})

test(`"!", "!easy!" ➞ 2`, () => {
    expect(charCount("!", "!easy!")).toBe(2)
})

test(`"wow", "the universe is wow" ➞ error`, () => {
    expect(() => charCount("wow", "the universe is wow")).toThrowError()
})

test(`" ", "the universe is wow" ➞ error`, () => {
    expect(charCount(" ", "the universe is wow")).toBe(3)
})

test(`"", "the universe is wow" ➞ error`, () => {
    expect(() => charCount( "", "the universe is wow")).toThrowError()
})

test(`"oh", "the universe is oh" ➞ error`, () => {
    expect(() => charCount("oh", "the universe is oh")).toThrowError()
})

test(`"🐕", "Woof woof 🐕 🐕 🐕" ➞ error`, () => {
    expect(() => charCount("🐕", "Woof woof 🐕 🐕 🐕")).toThrowError()
})