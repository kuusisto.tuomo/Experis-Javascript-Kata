export function rearrange(sentence)
{
    sentence = sentence.trim()
    const regex = /\d/; // Numbers
    let textArray = sentence.split(" ")
    textArray = textArray.sort((a,b) => a.match(regex) - b.match(regex))
    textArray = textArray.map(word => word.replace(word.match(regex), ""))
    return textArray.join(" ")
}