// rearrange("is2 Thi1s T4est 3a") ➞ "This is a Test"
// rearrange("4of Fo1r pe6ople g3ood th5e the2") ➞ "For the good of the people"
// rearrange("5weird i2s JavaScri1pt dam4n so3") ➞ "JavaScript is so damn weird"
// rearrange(" ") ➞ ""

import {rearrange} from "."

test(`"is2 Thi1s T4est 3a" ➞ "This is a Test"`, () => {
    expect(rearrange("is2 Thi1s T4est 3a")).toBe("This is a Test")
})

test(`"4of Fo1r pe6ople g3ood th5e the2 ➞ "For the good of the people"`, () => {
    expect(rearrange("4of Fo1r pe6ople g3ood th5e the2")).toBe("For the good of the people")
})

test(`5weird i2s JavaScri1pt dam4n so3" ➞ "JavaScript is so damn weird"`, () => {
    expect(rearrange("5weird i2s JavaScri1pt dam4n so3")).toBe("JavaScript is so damn weird")
})

test(`" " ➞ ""`, () => {
    expect(rearrange(" ")).toBe("")
})