/*
Basketball Points
You are counting points for a basketball game, given the amount of 2-pointers scored and 3-
pointers scored, find the final points for the team and return that value.
Examples
points(1, 1) ➞ 5
points(7, 5) ➞ 29
points(38, 8) ➞ 100
points(0, 1) ➞ 3
points(0, 0) ➞ 0
*/

import {points} from "."

test("1 x 2 pointer and 1 x 3 pointer should be equal to 5", () => {
    expect(points(1,1)).toBe(5)
})

test("7 x 2 pointer and 5 x 3 pointer should be equal to 29", () => {
    expect(points(7,5)).toBe(29)
})

test("38 x 2 pointer and 8 x 3 pointer should be equal to 100", () => {
    expect(points(38,8)).toBe(100)
})

test("0 x 2 pointer and 1 x 3 pointer should be equal to 3", () => {
    expect(points(0,1)).toBe(3)
})

test("0 x 2 pointer and 0 x 3 pointer should be equal to 0", () => {
    expect(points(0,0)).toBe(0)
})