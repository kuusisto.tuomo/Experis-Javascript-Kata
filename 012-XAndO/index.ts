export function XAndO(board) {
    let boardArray = board;
    let winningMove = [-1, -1];
    let foundMove = false;
    boardArray = board.map(row => row.split("|"))
    boardArray = [...boardArray[0], ...boardArray[1], ...boardArray[2]];
    let xPositions = boardArray.map((x, index) => x === "X" ? index + 1 : "").filter(n => n != "").join("");
    let oPositions = boardArray.map((o, index) => o === "O" ? index + 1 : "").filter(n => n != "").join("");
    const winningLines = ["123", "159", "147", "258", "357", "369", "456", "789"];
    for (let i = 1; i <= 9; ++i) {
        for (let j = 0; j < winningLines.length; ++j) {
            if (!oPositions.includes(i.toString()) && !xPositions.includes(i.toString())) {
                let resultArray = [...xPositions];
                resultArray.push(i.toString());
                let result = resultArray.filter(n => n != "").sort((a, b) => a - b).join("").toString();

                if (!foundMove) {
                    for (let n = 0; n < result.length; ++n) {
                        if (!winningLines[j].includes(result[n])) {
                            result = result.substring(0, n) + result.substring(n + 1);
                        }

                        if (result.includes(winningLines[j])) {
                            winningMove = [Math.floor((i - 1) / 3 + 1), (i - 1) % 3 + 1];
                            foundMove = true;
                            break;
                        }
                    }
                }
            }
        }
    }

    return winningMove;
}

// console.log(XAndO(["X| |X", "O|X|X", " | |O"]));