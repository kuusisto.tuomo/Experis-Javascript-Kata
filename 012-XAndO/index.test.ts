import {XAndO} from ".";

test(`board = [" | | ", " |X| ", "X| | "]) ➞ [1, 3]`,
    () => {expect(XAndO([" | | ", " |X| ", "X| | "])).toEqual([ 1, 3 ])} )

test(`board = ["X|X|O", "O|X| ", "X|O| "]) ➞ [3, 3]`,
    () => {expect(XAndO(["X|X|O", "O|X| ", "X|O| "])).toEqual([ 3, 3 ])} )

test(`board = [" | | ", " |X| ", " | | "]) ➞ [-1, -1]`,
    () => {expect(XAndO([" | | ", " |X| ", " | | "])).toEqual([ -1, -1 ])} )

test(`board = ["X| | ", "O|X|X", " | |O"])) ➞ [-1, -1]`,
    () => {expect(XAndO(["X| | ", "O|X|X", " | |O"])).toEqual([ -1, -1 ])} )

test(`board = ["X| |X", "O||X|X", " | |O"])) ➞ [1, 3]`,
    () => {expect(XAndO(["X| |X", "O||X|X", " | |O"])).toEqual([ 1, 2 ])} )