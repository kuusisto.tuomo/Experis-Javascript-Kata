import {reorder} from ".";

test(`"hA2p4Py" ➞ "APhpy24"`,
    () => {expect(reorder("hA2p4Py")).toEqual("APhpy24")} )

test(`"m11oveMENT" ➞ "MENTmove11"`,
    () => {expect(reorder("m11oveMENT")).toEqual("MENTmove11")} )

test(`"s9hOrt4CAKE" ➞ "OCAKEshrt94"`,
    () => {expect(reorder("s9hOrt4CAKE")).toEqual("OCAKEshrt94")} )

test(`"[ "Hello World" ]" ➞ error`,
    () => {expect(() => reorder(["Hello World"])).toThrowError()} )

test(`"!1Hasta 2Luego 3Amigo?" ➞ error`,
    () => {expect(() => reorder("!1Hasta 2Luego 3Amigo?")).toThrowError()} )

test(`"" ➞ ""`,
    () => {expect(reorder("")).toEqual("")} )
