 export function reorder(word) {
    if(typeof (word) != 'string') throw Error("Input is not a valid string");
    let arrayWord = word.split("");
    // Error check for non-alphanumeric input
     const regex = /[a-z0-9]/i;
     for(let i = 0; i < arrayWord.length; ++i){
        if(!arrayWord[i].match(regex)){
            throw Error(`${arrayWord[i]} is not a valid alphanumeric input!`);
        }
    }
    return arrayWord.sort((a, b) =>  charValue(b) - charValue(a)).join("");
}

function charValue(char){    // Capital letter value 3, lowercase letter value 2, number value 1
    if(!isNaN(char)) {
        return 1;
    }
    else if(char.toLowerCase() === char){
        return 2;
    }
    return 3;
}
