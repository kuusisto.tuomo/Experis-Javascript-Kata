import {howUnlucky} from ".";

test("2020  => 2", () => {expect(howUnlucky(2020)).toBe(2)} )
test("2026  => 3", () => {expect(howUnlucky(2026)).toBe(3)} )
test("2016  => 1", () => {expect(howUnlucky(2016)).toBe(1)} )
test("2016  => 1", () => {expect(howUnlucky(2016)).toBe(1)} )
test("-1  => error", () => {expect(() => howUnlucky(-1)).toThrowError()} )
