export function howUnlucky(year){
    if(typeof (year) != 'number' || year < 0) throw Error('Input is not a valid year');
    let unluckyFridays = 0;
    for(let i = 0; i < 12; ++i)
    {
        if(new Date(year,i,13).getDay() == 5){
            unluckyFridays++;
        }
    }
    return unluckyFridays;
}