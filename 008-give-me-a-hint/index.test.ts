import {grantTheHint} from "./index";

test(`("Mary Queen of Scots") ➞ ["____ _____ __ _____", "M___ Q____ o_ S____", "Ma__ Qu___ of Sc___", "Mar_ Que__ of Sco__", "Mary Quee_ of Scot_", "Mary Queen of Scots"]`, () =>{
    expect(grantTheHint("Mary Queen of Scots")).toEqual(
        ([
    "____ _____ __ _____",
    "M___ Q____ o_ S____",
    "Ma__ Qu___ of Sc___",
    "Mar_ Que__ of Sco__",
    "Mary Quee_ of Scot_",
    "Mary Queen of Scots"
    ]))
})

test(`("The Life of Pi") ➞ [
    "___ ____ __ __",
    "T__ L___ o_ P_",
    "Th_ Li__ of Pi",
    "The Lif_ of Pi",
    "The Life of Pi"
]`
    , () => {
    expect(grantTheHint("The Life of Pi")).toEqual(["___ ____ __ __", "T__ L___ o_ P_", "Th_ Li__ of Pi", "The Lif_ of Pi", "The Life of Pi"])
})