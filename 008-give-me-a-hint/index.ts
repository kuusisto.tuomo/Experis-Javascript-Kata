export function grantTheHint(sentence)
{
    if(typeof(sentence) !== "string") { throw new Error("Input is not a valid string")}
    let wordArray = sentence.split(" ")
    return revealCharacters(wordArray)
}

function revealCharacters(wordArray)
{
    let result = []
    let longestWord = wordArray.reduce((a, b) => a.length > b.length ? a : b).length

    for(let n = 0; n <= longestWord; ++n)
    {
        let revealed = wordArray.map(word => word.slice(0, n))
        for(let i = 0; i < wordArray.length; ++i)
        {
            for(let j = 0; j < wordArray[i].length - n; ++j)
            {
                revealed[i] += "_";
            }
        }

        result.push(revealed.join(" "))
    }

    return result
}

// console.log(grantTheHint("Mary Queen of Scots"))
//  console.log(grantTheHint("The Life of Pi"))