// correctTitle("jOn SnoW, kINg IN thE noRth") ➞ "Jon Snow, King in the North."
// correctTitle("sansa stark,lady of winterfell.") ➞ "Sansa Stark, Lady of
// Winterfell."
// correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.") ➞ "Tyrion Lannister,
// Hand of the Queen."

import {correctTitle} from "."

test(`("jOn SnoW, kINg IN thE noRth\") ➞ \"Jon Snow, King in the North.`, () => {
    expect(correctTitle(`jOn SnoW, kINg IN thE noRth`)).toBe(`Jon Snow, King in the North.`)
})

test(`("sansa stark,lady of winterfell.\") ➞ \"Sansa Stark, Lady of Winterfell.`, () => {
    expect(correctTitle(`sansa stark,lady of winterfell.`)).toBe(`Sansa Stark, Lady of Winterfell.`)
})

test(`("TYRION LANNISTER, HAND OF THE QUEEN") ➞ \"Tyrion Lannister, Hand of the Queen.`, () => {
    expect(correctTitle(`TYRION LANNISTER, HAND OF THE QUEEN`)).toBe(`Tyrion Lannister, Hand of the Queen.`)
})

test(`("TYRION LANNISTER , HAND OF THE QUEEN") ➞ \"Tyrion Lannister, Hand of the Queen.`, () => {
    expect(correctTitle(`TYRION LANNISTER , HAND OF THE QUEEN`)).toBe(`Tyrion Lannister, Hand of the Queen.`)
})

test(`("TYRION LANNISTER ,HAND OF THE QUEEN") ➞ \"Tyrion Lannister, Hand of the Queen.`, () => {
    expect(correctTitle(`TYRION LANNISTER ,HAND OF THE QUEEN`)).toBe(`Tyrion Lannister, Hand of the Queen.`)
})

test(`("TYRION LANNISTER    ,      HAND OF THE QUEEN") ➞ \"Tyrion Lannister, Hand of the Queen.`, () => {
    expect(correctTitle(`TYRION LANNISTER    ,      HAND OF THE QUEEN`)).toBe(`Tyrion Lannister, Hand of the Queen.`)
})

test(`("sansa stark, so-and-so lady of winterfell.\") ➞ \"Sansa Stark, So-and-So Lady of Winterfell.`, () => {
    expect(correctTitle(`sansa stark, so-and-so lady of winterfell`)).toBe(`Sansa Stark, So-and-So Lady of Winterfell.`)
})