const lowerCaseWords = ["and", "the", "of", "in"]

// Regex for fixing comma placement
const regex = /(,(?=\S))|(\s+,\s+)|(\s+,)/gs

export function correctTitle(providedTitle){
    // Force whole text to lowercase before fixing each word separately
    let correctedTitle = providedTitle.toLowerCase()

    // Fix commas with regex
    correctedTitle = correctedTitle.replace(regex, ", ")

    // Handle capitalization of words
    correctedTitle = (correctedTitle.split(" ").map(x => fixWordCapitalization(x))).join(" ")

    // Make sure there is a period at the end of the title
    correctedTitle = correctedTitle.endsWith(".") ? correctedTitle : correctedTitle + "."

    return correctedTitle
}

function fixWordCapitalization(word)
{
    // Handles hyphenated words as separate to follow capitalization rules
    return word.split("-").map(x => { return lowerCaseWords.includes(x) ? x : x.charAt(0).toUpperCase() + x.slice(1) }).join("-")
}