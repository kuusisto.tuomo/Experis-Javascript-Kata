// secondLargest([10, 40, 30, 20, 50]) ➞ 40
// secondLargest([25, 143, 89, 13, 105]) ➞ 105
// secondLargest([54, 23, 11, 17, 10]) ➞ 23
// secondLargest([1, 1]) ➞ 0
// secondLargest([1]) ➞ 1
// secondLargest([]) ➞ 0

import {secondLargest} from "."

test(`[10, 40, 30, 20, 50] ➞ second largest: 40`, () => {
    expect(secondLargest([10, 40, 30, 20, 50])).toBe(40)
})

test(`[25, 143, 89, 13, 105] ➞ second largest: 105`, () => {
    expect(secondLargest([25, 143, 89, 13, 105])).toBe(105)
})

test(`[54, 23, 11, 17, 10] ➞ second largest: 23`, () => {
    expect(secondLargest([54, 23, 11, 17, 10])).toBe(23)
})

test(`[1, 1] ➞ second largest: 0`, () => {
    expect(secondLargest([1, 1])).toBe(0)
})

test(`[1] ➞ second largest: 1`, () => {
    expect(secondLargest([1])).toBe(1)
})

test(`[] ➞ second largest: 0`, () => {
    expect(secondLargest([])).toBe(0)
})

test(`[54, 54, 11, 54, 11, "cat", "dog"] ➞ second largest: 11`, () => {
    expect(secondLargest([54, 54, 11, 54, 10, 11])).toBe(11)
})

test(`[5, 5, 5, 5, 5, 5] ➞ second largest: 5`, () => {
    expect(secondLargest([5, 5, 5, 5, 5, 5])).toBe(5)
})

test(`[1, 8, 54, 67, -15, "sheep", 1.05] ➞ second largest: 54`, () => {
    expect(secondLargest([1, 8, 54, 67, -15, "sheep", 1.05])).toBe(54)
})

test(`["dog", "sheep", 1, "shoe"] ➞ second largest: 1`, () => {
    expect(secondLargest(["dog", "sheep", 1, "shoe"])).toBe(1)
})

test(`["dog", "sheep", 1, "shoe", 1] ➞ second largest: 0`, () => {
    expect(secondLargest(["dog", "sheep", 1, "shoe", 1])).toBe(0)
})

test(`["dog", "sheep"] ➞ second largest: 0`, () => {
    expect(secondLargest(["dog", "sheep"])).toBe(0)
})