export function secondLargest(array){
    if(!Array.isArray(array)) throw "Input is not an array"
    array = filterNumbers(array)
    // Return 0 for an empty array
    if(array.length == 0)
    {
        return 0
    }
    // If only one number exists, return that number
    else if(array.length == 1)
    {
        return array[0]
    }
    // When array only has two numbers that are equal, return 0
    else if(array.length == 2 && array[0] === array[1])
    {
        return 0
    }
    else
    {
        let sorted = array.sort((a,b) => b - a) // "Bubble sort"
        const uniqueValues = new Set(sorted)
        return uniqueValues.size > 1 ? Array.from(uniqueValues)[1] : Array.from(uniqueValues)[0]
    }
}

function filterNumbers(array)
{
    let filtered = array
    for(let i = filtered.length - 1; i >= 0; --i)
    {
        if(isNaN(filtered[i]))
        {
            filtered.splice(i,1)
        }
    }

    return filtered
}