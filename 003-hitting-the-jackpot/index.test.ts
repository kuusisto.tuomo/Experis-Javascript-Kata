/*
testJackpot(["@", "@", "@", "@"]) ➞ true
testJackpot(["abc", "abc", "abc", "abc"]) ➞ true
testJackpot(["SS", "SS", "SS", "SS"]) ➞ true
testJackpot(["&&", "&", "&&&", "&&&&"]) ➞ false
testJackpot(["SS", "SS", "SS", "Ss"]) ➞ false
*/

import {testJackpot} from "."
// const {testJackpot } = require ("./main")


test("4 x @ should be a jackpot", () => {
    expect(testJackpot(["@", "@", "@", "@"])).toBe(true)
})

test("4 x abc should be a jackpot", () => {
    expect(testJackpot(["abc", "abc", "abc", "abc"])).toBe(true)
})

test("4 x SS should be a jackpot", () => {
    expect(testJackpot(["SS", "SS", "SS", "SS"])).toBe(true)
})

test("Different &'s should not be a jackpot", () => {
    expect(testJackpot(["&&", "&", "&&&", "&&&&"])).toBe(false)
})

test("Different SS's should not be a jackpot", () => {
    expect(testJackpot(["SS", "SS", "SS", "Ss"])).toBe(false)
})

test(`"hello world" is not a valid input`, () =>
{
    expect(() => testJackpot("hello world")).toThrowError()
})