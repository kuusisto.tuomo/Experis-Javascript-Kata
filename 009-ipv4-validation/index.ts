// IPV4 Contains 3 periods and 4 octets.
// Octets are decimal values between 0 and 255, and they are separated by periods.
export function isValidIP(ip){
    if(typeof(ip) !== "string") throw new Error("Input is not a valid string");

    let result = true;

    let octetArray = ip.split("."); // Split octets into an array

    if((octetArray.length) != 4){ // Check if there is the correct amount of octets
        result = false;
    }
    else {
        const regex = new RegExp(/^\d+$/); // Matches one digit at the start of the string (^) and matches one or more at the end (+$)
        octetArray.forEach((octet, index) => {
            if(regex.test(octet) === false){ // Check that the string contains only numbers
                result = false;
            } else if (octet[0] === "0") { // Leading zeroes are not valid in octet.
                result = false;
            } else if (parseInt(octet) < 0 || parseInt(octet) > 255) { // Values may only be between 1 and 255.
                result = false;
            } else if (index == 3 && (parseInt(octet) % 10 == 0)) { // Last digit may not be zero (check with remainder operator)
                result = false;
            }
        })
    }

    return result;
}