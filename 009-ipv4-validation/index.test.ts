import {isValidIP} from "."

const test1 = "1.2.3.4"; // true
const test2 = "1.2.3"; // false
const test3 = "1.2.3.4.5"; // false
const test4 = "123.45.67.89"; // true
const test5 = "123.456.78.90"; // false
const test6 = "123.045.067.089"; // false

test(`${test1} ➞ true`, () => {
    expect(isValidIP(test1)).toBe(true)
})

test(`${test2} ➞ false`, () => {
    expect(isValidIP(test2)).toBe(false)
})

test(`${test3} ➞ false`, () => {
    expect(isValidIP(test3)).toBe(false)
})

test(`${test4} ➞ false`, () => {
    expect(isValidIP(test4)).toBe(true)
})

test(`${test5} ➞ false`, () => {
    expect(isValidIP(test5)).toBe(false)
})

test(`${test6} ➞ false`, () => {
    expect(isValidIP(test6)).toBe(false)
})

test(`${"hello.world"} ➞ false`, () => {
    expect(isValidIP("hello.world")).toBe(false)
})

test(`${"123.4-2.3.56"} ➞ false`, () => {
    expect(isValidIP("123.4-2.3.56")).toBe(false)
})

test(`${""} ➞ false`, () => {
    expect(isValidIP("")).toBe(false)
})

test(`${"123.123.123.12"} ➞ true`, () => {
    expect(isValidIP("123.123.123.12")).toBe(true)
})

test(`${"123.-123.123.12"} ➞ false`, () => {
    expect(isValidIP("123.-123.123.12")).toBe(false)
})



