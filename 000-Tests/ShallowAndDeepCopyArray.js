const sheep = ["s", "s", "s"]
const sheepShallowCopy = sheep
const sheepDeepCopy = [...sheep]

console.log(sheep)
console.log(sheepShallowCopy)
console.log(sheepDeepCopy)

console.log("Wolf added to original array")
console.log("Sheep added to shallow copy array")
console.log("Fox added to deep copy array")

sheep.push("w")
sheepShallowCopy.push("s")
sheepDeepCopy.push("f")

console.log(sheep)
console.log(sheepShallowCopy)
console.log(sheepDeepCopy)